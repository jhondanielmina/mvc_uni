package com.universidad.vista;

import javax.swing.JOptionPane;

import com.universidad.controlador.ControladorUniversidad;
import com.universidad.modelo.Universidad;

public class Registro {

    //Atributos
    ControladorUniversidad cUniversidad;
    
    /***************
     * Constructor
     **************/
    public Registro(){
        this.cUniversidad = new ControladorUniversidad();
        this.menu();
    }

    //Menu
    public void menu(){

        int opc=-1;
        String message="Ingrese 1 para ingresar universidad";
        message+="\nIngrese 2 para ver todas las universidades";
        message+="\nIngrese 0 para salir";
        while(opc != 0){

            opc=Integer.parseInt(JOptionPane.showInputDialog(null, message));
            if(opc  == 1){
                solicitar_datos();
            }

            else if(opc == 2){

                 
                obtenerUniversidades();


            }

        }

    }

    //Métodos/Acciones
    public void solicitar_datos(){
        //Solicitar los datos
        String nombre = JOptionPane.showInputDialog(null, "Por favor ingrese el nombre de la universidad");
        String direccion = JOptionPane.showInputDialog(null, "Por favor ingrese la dirección de la universidad "+nombre);
        String nit = JOptionPane.showInputDialog(null, "Por favor ingrese el nit de la universidad "+nombre);
        //Creamos la universidad
        this.cUniversidad.crearUniversidad(nombre, direccion, nit);
    }

    public void obtenerUniversidades(){
        String infoUniversidad="";
                // String direccion;
                 //String nit;

                 for(Universidad Universidad: this.cUniversidad.getUniversidades()){

                    infoUniversidad+="\nNombre universidad: "+Universidad.getNombre();
                    infoUniversidad+="\nDirección universidad: "+Universidad.getDireccion();
                    infoUniversidad+="\nNit universidad: "+Universidad.getNit();
                    infoUniversidad+="\n--------------------------------------------- ";
                    

                 }

        JOptionPane.showMessageDialog(null, infoUniversidad);


    }


}
