package com.universidad.controlador;

import java.util.ArrayList;

import com.universidad.modelo.Universidad;

public class ControladorUniversidad {
    
    //private Universidad[] universidades;
    private ArrayList<Universidad> arrayUniversidad;

    public ControladorUniversidad(){
        //this.universidades = new Universidad[2];
        //Arreglo dinámico
        this.arrayUniversidad = new ArrayList<Universidad>();
    }

    public Universidad getUniversidad(int pos){
        //return this.universidades[pos];
        return this.arrayUniversidad.get(pos);
    }

    public ArrayList<Universidad> getUniversidades(){
        return this.arrayUniversidad;
    }

    public void crearUniversidad(String nombre, String direccion, String nit){
        //this.universidades[pos] = new Universidad(nombre, direccion, nit);
        Universidad objUniversidad = new Universidad(nombre, direccion, nit);
        this.arrayUniversidad.add(objUniversidad);
    }

}
